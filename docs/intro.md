---
sidebar_label: 'Account Registration'
sidebar_position: 1
id: intro
title: Account Registration
description: Einrichtung des SG-News Accounts
slug: /intro
---

# Account Registration

I like **dinos** btw!

## Email Adresse einrichten

:::danger Achtung!

Falls du noch keine Emailadresse erhalten hast, melde dich bei einem [Entwickler/Administrator](mailto:dev@sgnews.de)

:::

Jedes SG-News Mitglied erhält eine Emailadresse.
Da aber Administratoren sich nicht um die Emailpostfächer kümmern wollen... äh können, werden die Emails an ein bereits existentes Emailfach weitergeleitet.

Die Emails, die an m.mustermann@sgnews.de gesendet werden, kommen beispielsweise im m.muster69@gmail.com an.
Die Emailadresse folgt dem Muster der iServ Login Daten.

Um Emails über die sgnews.de Adresse zu erhalten, musst du zuvor in der Bestätigungsemail verifizieren, dass wir mit deiner Emailadresse verkaufen... ich meine, dass die Emails weitergeleitet werden dürfen.

![Emailadresse Bestätigung](/img/email_verification.png)
Ungefähr so sollte die Email aussehen.

ASO UND NEIN IHR MÜSST EUCH NICHT BEI CLOUDFLARE EINLOGGEN!
Danke :)
